import setuptools

setuptools.setup(
    name="iiwml",
    version="0.0.0",
    author="Felix Tim Boelle",
    author_email="feltbo@dtu.dk",
    description="Run the Ion insertion workflow autonomously",
    maintainer='Felix Tim Boelle',
    maintainer_email='feltbo@dtu.dk',
    url="https://gitlab.com/Feltbo/iiwml",
    packages=setuptools.find_packages(),
    install_requires=[
        'pytest',
        'pytest-cov',
        'ase>=3.19.1',
        'pymatgen>=2020.4.2',
        'pulp>=2.3',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: Unix",
    ],
    python_requires='>=3.6',
)
