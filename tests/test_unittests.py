" Small and quick unit tests using pytest"
import numpy as np

from ase.build import fcc111, bulk
from ase.io import read


class TestDescriptors:
    """ Test all descriptors defined """

    def test_gravimetric_capacity(self):
        from iiwml.descriptors.basic import get_gravimetric_capacity
        atoms = fcc111('Cu', [7, 1, 1], a=3)
        atoms.set_chemical_symbols(['Li', 'Fe', 'P', 'O', 'O', 'O', 'O'])
        Q_th_g = get_gravimetric_capacity(atoms, ion='Li')

        assert np.isclose(Q_th_g, 169.893)

    def test_volumetric_capacity(self):
        from iiwml.descriptors.basic import get_volumetric_capacity
        atoms = fcc111('Cu', [7, 1, 1], a=4, vacuum=3)
        atoms.set_chemical_symbols(['Li', 'Fe', 'P', 'O', 'O', 'O', 'O'])
        Q_th_v = get_volumetric_capacity(atoms, ion='Li')

        assert np.isclose(Q_th_v, 152.946)

    def test_cnn_voronoi_covalent(self):
        from iiwml.descriptors.atom_env import get_cnn_voronoi_covalent
        atoms = bulk('Cu', 'bcc', a=3.5)
        n = get_cnn_voronoi_covalent(atoms, ind=0, spec_neighbors=False)
        assert n == 8

        atoms = bulk('Cu', 'fcc', a=3.0)
        n = get_cnn_voronoi_covalent(atoms, ind=0, spec_neighbors=False)
        assert n == 12

        atoms = bulk('Cu', 'fcc', a=3.0)
        atoms = atoms.repeat([2, 1, 1])
        atoms.set_chemical_symbols(['O', 'Cu'])
        # bonding to itself
        n = get_cnn_voronoi_covalent(atoms, ind=1, spec_neighbors=['Cu'])
        assert n == 6
        n = get_cnn_voronoi_covalent(atoms, ind=1, spec_neighbors=['O'])
        assert n == 6

    def test_cnn_voronoi_mindis(self):
        from iiwml.descriptors.atom_env import get_cnn_voronoi_mindis
        atoms = bulk('Cu', 'bcc', a=4.5)
        n = get_cnn_voronoi_mindis(atoms, ind=0, spec_neighbors=False)
        assert n == 8

        atoms = bulk('Cu', 'fcc', a=3.0)
        n = get_cnn_voronoi_mindis(atoms, ind=0, spec_neighbors=False)
        assert n == 12

        atoms = bulk('Cu', 'fcc', a=3.0)
        atoms = atoms.repeat([2, 1, 1])
        atoms.set_chemical_symbols(['O', 'Cu'])
        # bonding to itself
        n = get_cnn_voronoi_mindis(atoms, ind=1, spec_neighbors=['Cu'])
        assert n == 6
        n = get_cnn_voronoi_mindis(atoms, ind=1, spec_neighbors=['O'])
        assert n == 6
        
    def test_lattice_features(self):
        from iiwml.descriptors.lattice import LatticeFeatures
        atoms = fcc111('Cu', [1, 1, 1], a=3, periodic=True)
        lf = LatticeFeatures(atoms)
        
    def test_ewald_energy(self):
        from iiwml.descriptors.electrostatic import get_ewald_energy
        atoms = bulk('NaCl', 'rocksalt', a=3.5, cubic=True)
        ewald_energy = get_ewald_energy(atoms, {'Na': 1, 'Cl': -1})

class TestVoronoiAnalyzer:
    """ Functions concerning voronoi topology analyzer """

    def test_find_possible_faces(self):
        from iiwml.descriptors.topology import find_possible_faces
        atoms = read('./structures/Mg2Mn4O8_mp-27510_spinel.traj')
        ps = atoms.get_positions()
        r_crits, f_chebyshevs, f_els, f_d_el_cheb, f_inds = find_possible_faces(
            atoms, ind_pos=8, ind_final=9, ion='Mg')

        # symmetric, so all r_crits should be same
        assert all(r_crits - min(r_crits) < 1e-4)

        d_chb_ref = np.linalg.norm(ps[8] - f_chebyshevs, axis=1)
        
        d_is = np.array([0.65958056, 0.65957465, 0.65957765, 0.65957631])
        crits = np.array([0.61066596, 0.61066511, 0.61066596, 0.61066511])
        p_chb_0 = np.array([-1.74806490e-06,
                             1.99175449e+00,
                             3.83989921e+00])
        
        assert np.allclose(d_chb_ref, d_is)
        assert np.allclose(r_crits, crits)
        assert np.allclose(f_chebyshevs[0], p_chb_0, atol=1e-6)
        assert len(f_inds) == len(f_els)
        
    def test_find_interstitials(self):
        from ase.visualize import view
        from iiwml.descriptors.topology import find_interstitials
        atoms = read('./structures/Mg2Mn4O8_mp-27510_spinel.traj')
        inters_ps = find_interstitials(atoms, 
                                       ind_init=8,
                                       filter_out=False)

        # just to test if it works, by no means is that number necessary
        # if we find a better interstitial algorithm, this will be replaced
        assert len(inters_ps) == 18

        inters_ps = find_interstitials(atoms, 
                                       ind_init=8,
                                       filter_out=True)
        assert len(inters_ps) == 18 # does not filter out for 65% but for 70%

        
