# iiwml

The iiwml or "ion insertion workflow - machine learning" package was originally build on top of the ion insertion workflow package. It tries to find useful descriptors for describing migration barriers in bulk materials. 

The methods have been applied and described in detail in the underlying paper:
https://doi.org/10.1002/batt.202100086

For more info on how to create DFT+NEB data automatically see also the the ion insertion workflow:
https://gitlab.com/asc-dtu/workflows/ion-insertion-battery-workflow

# Setup
Clone this repositorsy, create a virtual environment using your favorite tool and run inside the cloned repository

```
pip install --editable .
```

# Example
This is a basic example executing the algorithm described in the underlying paper in Figure 2.
```python
from ase.io import read
from iiwml.topology import topology_voronoi_path, visualize_path_from_path_list

supercell = read('spinel_supercell.traj')
unit_cell = read('spinel_unit_cell.traj')

# this executes the algorithm described in the paper
path_list = topology_voronoi_path(
    supercell,
    ind_init=48,  # indice of initial position in supercell
    ind_final=51,  # indice of final position in supercell
    unit_cell=unit_cell,  # optional, for performance reasons
    reapeat=[2, 2, 2],  # how was unit_cell repeated to get supercell
    )

# visualize the path using ASE
visualize_path_from_path_list(supercell, path_list)
```
