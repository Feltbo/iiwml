"""
Script for storing electrostatic descriptor functions for battery materials

@authors: Felix Tim Bölle <feltbo@dtu.dk>
"""

from pymatgen.analysis.ewald import EwaldSummation
from pymatgen.io.ase import AseAtomsAdaptor

import iiwml.data as iiw_data

def get_ewald_energy(atoms, ox_states):
    """ Returns the total energy from Ewald summation 
    
    Parameters
    ------------
    atoms : obj[ase.atoms]
    ox_states : dic
        Contains user defined oxidation states of elements in atoms
    """
    adapt = AseAtomsAdaptor()
    structure = adapt.get_structure(atoms)
    structure.add_oxidation_state_by_element(ox_states)
    ewald = EwaldSummation(structure)
    ewald_energy = ewald.total_energy
    
    return ewald_energy