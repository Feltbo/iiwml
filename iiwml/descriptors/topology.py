"""
Topology descriptors

@authors: Felix Tim Bölle <feltbo@dtu.dk>
"""
from operator import itemgetter

import numpy as np
from scipy.spatial import Voronoi, ConvexHull

import pulp

from pymatgen.analysis.local_env import solid_angle, VoronoiNN
from pymatgen.io.ase import AseAtomsAdaptor
from pymatgen.core.periodic_table import Element
from pymatgen.analysis.defects.utils import TopographyAnalyzer


from ase.geometry import get_distances, wrap_positions
from ase.neighborlist import neighbor_list
from ase.atoms import Atom, Atoms
from ase.visualize import view
from ase.data import atomic_numbers, covalent_radii

from iiwml.descriptors.voronoi import create_sphere_from_neighbor_list,\
                                      get_el_coordination_voronoi
from iiwml.descriptors.atom_env import get_cnn_voronoi_mindis,\
                                       get_cnn_voronoi_covalent,\
                                       mindis_nn
from iiwml.data import max_ox_state_metals, non_metal_ox_states

import matplotlib.pyplot as plt


def build_voronoi_partition(atoms, ref_pos, cutoff_ngb=10,
                            visualize=False):
    """
    Basic function to create Voronoi partition

    Parameters
    ----------
    atoms : ase.atoms
    ref_pos : ndarray
        Build voronoi partition around this point
    cutoff_ngb : flot
        cutoff value for neighbor list search

    Returns
    -------
    vor : scipy.spatial.Voronoi
        The voronoi object
    ind : int
    ngb_out : list
        i, j, d , D output form ase's neighbors list
    """
    # including pbc using ase.neighborlist
    i, j, d, D = neighbor_list('ijdD', atoms, cutoff=cutoff_ngb)
    
    # build the voronoi partition around ind closest to ref_pos
    ps = atoms.get_positions()
    vecs, dists = get_distances(np.array(ref_pos), ps, cell=atoms.cell, pbc=True)

    ind = np.argmin(dists)
    mask = i == ind
    points = [[0, 0, 0]]  # place element ind at origin
    for vec in D[mask]:
        points.append(vec)

    vor = Voronoi(points)
    visualize = False
    if visualize:
        visualize_voronoi(atoms, ind, i, j, D, vor)

    ngb_out = [i, j, d, D]
    return vor, ind, ngb_out


def visualize_voronoi(atoms, ind, i, j, D, vor):
    from ase.visualize import view
    atoms = atoms.copy()
    sphere = create_sphere_from_neighbor_list(atoms, ind, i, j, D)
    view(sphere)
    
    for vor_point in vor.vertices:
        if np.linalg.norm(vor_point) > 10:
            continue
        sphere.append(Atom('H', position=vor_point))
    view(sphere)
    import sys;sys.exit()
    
def path_midpoint(atoms, ind_init, ind_final):
    atoms = atoms.copy()
    neb_vec = atoms.get_distance(ind_init, ind_final, mic=True, vector=True)
    init_vec = atoms.get_positions()[ind_init]
    midpoint_vec = init_vec + neb_vec/2
    # make sure to wrap into cell again if across pbcs
    atoms.append(Atom('Xe', position=midpoint_vec))
    atoms.wrap()

    
    midpoint = atoms.get_positions()[-1]
    return midpoint


def get_voronoi_pymatgen(atoms):
    adapt = AseAtomsAdaptor()
    structure = adapt.get_structure(atoms)
    
    pmvor = VoronoiNN()
    all_poly = pmvor.get_all_voronoi_polyhedra(structure)
    return all_poly


def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + np.cos(angle) * (px - ox) - np.sin(angle) * (py - oy)
    qy = oy + np.sin(angle) * (px - ox) + np.cos(angle) * (py - oy)
    return qx, qy


def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

    Notes
    -------
    This handles cases when vectors have same/opposite directions
    """
    v1_u = normalize(v1)
    v2_u = normalize(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def intersect2D(a1,a2, b1,b2):
    """ Find intersecting point given two arrays """
    a1 = np.array(a1)
    b1 = np.array(b1)
    a2 = np.array(a2)
    b2 = np.array(b2)
    d_a = a2-a1
    d_b = b2-b1
    dp = a1-b1
    dap = perpendicular(d_a)
    denom = np.dot(dap, d_b)
    num = np.dot(dap, dp )
    return (num / denom.astype(float))*d_b + b1

def investigate_ngb_faces(vor, neighb_points, ngb_els, ref_pos):
    """
    Find r_critical in a polyhedra

    Parameters
    ----------
    vor : scipy.spatial.Voronoi
        Voronoi partitioned space
    ngb_els : list[str]
        all elements matching voronoi partition
    ref_pos : ndarray
        Reference position that voronoi partitioned was build around

    Returns
    -------
    r_crits : list[float]
        Radius of largest ball fitting into face
    f_chebyshevs : list[ndarray]
        Chebyshev center coordinates of each face in 3D unit cell
    faces_elements : list[str]
        elements occupying edges of face
    faces_dists_el_cheb : list[ndarray]
        containing list with distance of edge elements to chebyshev center

    """
    r_crits = []
    f_chebyshevs = []
    faces_elements = []
    faces_dists_el_cheb = []
    polyhedra_3D = []

    n_vs, areas, polyhedra_2D, polyhedra_3D_coords, simplices, elements, bvps = build_polyhedra(
        vor.points[neighb_points], ngb_els)

    for i, (polyhedron, edge_els) in enumerate(list(zip(polyhedra_2D, elements))):
        # visit each point and calculate new points using edge_el radius
        poly_info = list(zip(polyhedron, edge_els))
        
        c_r, c_x, c_y = center_polyhedron_elements(polyhedron, edge_els)
        twoD_center = np.array([c_x, c_y])
        
        center_3D = project_2Dcenter_on_3Dfacet(bvps[i][0], bvps[i][1],
            twoD_center=twoD_center,
            threeD_points=polyhedra_3D_coords[i])
        
        r_crits.append(c_r)
        # need to translate back from voronoi partition coorinates into
        # unit cell coordinates - add ref_pos to center!
        f_chebyshevs.append(center_3D + ref_pos)
        
        # store information on environment
        faces_elements.append(edge_els)
        # distance to each edge element
        dists = np.linalg.norm(polyhedron - twoD_center, axis=1)
        faces_dists_el_cheb.append(dists)
        polyhedra_3D.append(polyhedra_3D_coords[i] + ref_pos)

    return [np.array(r_crits), 
            np.array(f_chebyshevs),
            faces_elements,
            faces_dists_el_cheb,
            polyhedra_3D]

def polygon_2D_anticlockwise(x, y):
    area = 0.5* (np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))
    if area > 0:
        return True
    return False

def PolyArea(x,y):
    """ This implements the Shoelace formula (e.g. wikipedia) """
    return 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))


def center_polyhedron_elements(polyhedron, edge_els, plot=False):
    """ Find center/largest circle of polyhedron respecting element radii 
    
    Parameters
    -----------
    polyhedron : ndarray
        Contains the edges of the 2D polyhedron clockwise or anticlockwise
    
    Notes
    ------
    This is solved in 2D.
    """
    
    if plot:
        plt.figure()
        ax = plt.gca()
    
    tangent_triangle = []

    if polygon_2D_anticlockwise(x=polyhedron[:, 0], y=polyhedron[:, 1]):
        polyhedron = polyhedron[::-1]  #  reverse the order
    
    poly_info = list(zip(polyhedron, edge_els))
    for i, (edge_point, edge_el) in enumerate(poly_info):
        p_ngb_next = polyhedron[get_index(i, len(polyhedron), 'next')]
        p_ngb_prev = polyhedron[get_index(i, len(polyhedron), 'previous')]
        v1 = p_ngb_next - edge_point
        v2 = p_ngb_prev - edge_point
        angle_rad = angle_between(v1, v2)
        rot_rad = np.deg2rad((np.rad2deg(angle_rad)/2))
        
        # length equal to ionic radius
        ox_state = {**max_ox_state_metals, **non_metal_ox_states}[edge_el]
        r_el = get_radius_ion(edge_el, ox_state)

        # now rotate point into middle
        n_v1 = normalize(v1)

        unr_p = edge_point + r_el * n_v1
        rot_p = np.array(rotate(
            origin=edge_point, point=unr_p, angle=rot_rad))
        
        # get tangent vector on point
        t_v_dir = normalize(perpendicular(rot_p - edge_point))
        t_p_2 = rot_p + t_v_dir*2
        
        tangent_triangle.append((rot_p, t_p_2))
        
        if plot:
            ax.add_artist(plt.Circle(edge_point, r_el, fill=False))
            plt.scatter(rot_p[0], rot_p[1], c='green')
            plt.plot(polyhedron[:, 0], polyhedron[:, 1], c='b')
            plt.plot([polyhedron[0, 0], polyhedron[-1, 0]],
                     [polyhedron[0, -1], polyhedron[-1, -1]], c='b')
    
    inner_poly = []
    for i, (a1, a2) in enumerate(tangent_triangle):
        b1, b2 = tangent_triangle[get_index(i, len(tangent_triangle), 'next')]
        inner_poly.append(intersect2D(a1,a2, b1,b2))
    
    # get center of polyhedron
    inner_poly = np.array(inner_poly)
    c_r, c_x, c_y = get_chebychev_center(inner_poly)
    
    if plot:
        plt.scatter(inner_poly[: ,0], inner_poly[:, 1])
        plt.plot(inner_poly[: ,0], inner_poly[:, 1], c='orange')
        plt.plot([inner_poly[0, 0], inner_poly[-1, 0]],
                 [inner_poly[0, -1], inner_poly[-1, -1]], c='orange')
        ax.add_artist(plt.Circle((c_x, c_y), c_r, fill=False, color='red'))
        ax.set_aspect('equal')
    
    return c_r, c_x, c_y


def get_chebychev_center(polyhedron):
    """ Return the center of any polygon being largest circle possible
    
    Parameters
    ------------
    polyhedron : ndarray
        array containing vertices, e.g. [(1,1), (2,5), (5,4), (6,2), (4,1)]
    
    Returns
    ---------
    r : float
        radius of largest circle
    x, y : floats
        center of circle having largest radius in polyhedron
    
    Notes
    --------
    We use pulp since it is straightforward, if time use scipy following
    https://www.researchgate.net/publication/335174848_NumPy_SciPy_Recipes_for_Data_Science_Linear_Programming
    """
    
    if not polygon_2D_anticlockwise(x=polyhedron[:, 0], y=polyhedron[:, 1]):
        polyhedron = polyhedron[::-1]  #  reverse the order
        
    # create input variables
    vertices = polyhedron
    # need a vstack here
    edges = zip(vertices, np.vstack((vertices[1:], [vertices[0]])))
    
    # Create the model
    model = pulp.LpProblem(name="chebyshev-center", sense=pulp.LpMaximize)
    
    # Initialize the decision variables
    r = pulp.LpVariable(name="r")
    x = pulp.LpVariable(name="x")  # default lowBound=-inf
    y = pulp.LpVariable(name="y")  
    
    # Add the constraints to the model
    for i, ((x1, y1), (x2, y2)) in enumerate(edges):
        dx = x2 - x1
        dy = y2 - y1
        model += ((dx*y - dy*x) + (r * np.sqrt(dx**2 + dy**2)) <= dx*y1 - dy*x1,
                  f"constraint_edge_{i}",
                 )
        
    # Add the objective function to the model
    model += r
    
    # Solve the problem using which ever pulp solver is available
    default_solver = get_pulp_solver()
    status = model.solve(default_solver(msg=0))  # suppress output
    
    # extract variables
    c_r = model.variables()[0].value()
    c_x = model.variables()[1].value()
    c_y = model.variables()[2].value()
    
    return c_r, c_x, c_y

def get_pulp_solver():
    """ Work around the problem of different naming for default solver
    
    Notes
    -------
    work around this issue: 
    https://stackoverflow.com/questions/49564890/pulp-not-working-with-travis-ci
    """
    available = pulp.list_solvers(onlyAvailable=True)
    if not available:
        raise ImportError("No solver available for pulp")
    solver_str = available[0]
    solver = getattr(pulp, solver_str)
    return solver


def get_radius_ion(el, ox_state):
    """ Use pymatgen ionic radii and self defined oxidation states """
    try:
        r_el = Element(el).ionic_radii[ox_state]
    except KeyError:  
        # oxidation state radius not available, choose closest available
        ox_aval = Element(el).ionic_radii.keys()
        ox_closest = min(ox_aval, key=lambda x:abs(x - ox_state))
        r_el = Element(el).ionic_radii[ox_closest]
        
    return float(r_el)

def get_index(i, len_list, direction):
    if direction == 'next':
        if i == len_list - 1:
            index = 0
        else:
            index = i + 1
    
    if direction == 'previous':
        if i == 0:
            index = -1
        else:
            index = i -1
            
    return index
    
def normalize(v):
    norm = np.linalg.norm(v)
    if norm == 0: 
       return v
    return v / norm

def perpendicular(v):
    b = np.empty_like(v)
    b[0] = -v[1]
    b[1] = v[0]
    return b
    

def build_polyhedra(points, ngb_els):
    """ Return a polyhedron given a set of points 
    
    Returns
    -------
    n_vs : ndarray
        Normal vectors through faces in 3D space
    areas : ndarray[floats]
        Area of face measured from midpoint of vertex elements
    polyhedra : ndarray
        array containing edge points of all faces
    elements : list[str]
        Elements matching edge points of all faces
        
    """
    already_2d = False
    areas = []
    n_vs = []  # normal vectors
    polyhedra_2D = []
    elements = []
    polyhedra_3D = []
    bvps = []
    inds = []
    
    hull = ConvexHull(points)
    hull_points = hull.points
    simplices = hull.simplices

    
    for facet_inds in simplices:  # all the inds of all faces
        polyhedron, n, bvp1, bvp2 = project_3Dpolygon_on_2Dplane(
            hull_points[facet_inds])
        areas.append(PolyArea(x=polyhedron[:, 0], y=polyhedron[:, 1]))
        n_vs.append(n)
        polyhedra_2D.append(polyhedron)
        polyhedra_3D.append(hull_points[facet_inds])
        elements.append(np.array(ngb_els)[facet_inds])
        bvps.append([bvp1, bvp2])
        
    return n_vs, areas, polyhedra_2D, polyhedra_3D, simplices, elements, bvps


def project_2Dcenter_on_3Dfacet(bvp1, bvp2, twoD_center, threeD_points):
    """ Project the 2D projected point back onto the orginial plane
    
    Notes
    ------
    bvp1 and bvp2 are the orthogonal vectors lying on the plane used
    to project the 3D points onto the 2D plane initially.
    see function `project_3Dpolygon_on_2Dplane`
    """
    r_o = origin_3D_polygon(threeD_points)
    x, y = twoD_center
    threeD_center = r_o + normalize(bvp1) * x + normalize(bvp2) * y
    return threeD_center


def origin_3D_polygon(threeD_points):
    return threeD_points[0]

def project_3Dpolygon_on_2Dplane(threeD_points):
    """ return 2D point data from a 3D shaped polygon lying on a plane """
    # find vector normal, first choose 3 points of polygon
    p1, p2, p3 = threeD_points[:3]
    
    # define two vectors that are in the plane
    v1 = p3 - p1
    v2 = p2 - p1
    
    # get normal to the plane
    vn = normalize(np.cross(v1, v2))
    # get two perpendicular vectors on plane
    bvp1 = normalize(v1)
    # get perpendicular vector to v1
    bvp2 = normalize(np.cross(v1, vn))

    r_O = origin_3D_polygon(threeD_points)  # define origin on plane
    twoD_points = []
    for threeD_point in threeD_points:
        twoD_point = point_on_plane(threeD_point, bvp1, bvp2, vn, r_O)
        twoD_points.append(twoD_point)

    return np.array(twoD_points), vn, bvp1, bvp2
    
def point_on_plane(r_P, bvp1, bvp2, vn,  r_O):
    """ 
    https://stackoverflow.com/questions/23472048/projecting-3d-points-to-2d-plane
    """
    # target point must obey r_P = r_O + t_1*e_1 + t_2*e_2 + s*n
    s = np.dot(vn, r_P-r_O)  # out of plane separation
    t_1 = np.dot(bvp1, r_P-r_O)    
    t_2 = np.dot(bvp2, r_P-r_O)
    r_P_plane = np.array([t_1, t_2])
    return r_P_plane

def is_parallel(v1, v2, eps=1e-6):
    return np.dot(v1,v2)/(np.linalg.norm(v1)*np.linalg.norm(v2)) > 1 - eps


def get_all_faces_voronoi(vor, ind):
    faces = []
    neighb_points = []
    neighb_ps_dists = []
    for i, points in enumerate(vor.ridge_points):
        if ind in points:
            face_inds = vor.ridge_vertices[i]
            face_points = np.array(vor.vertices)[face_inds]
            faces.append(face_points)
            ngb_ind = [p for p in points if p != ind][0]
            neighb_points.append(ngb_ind)
            dist = np.linalg.norm(vor.points[ind] - vor.points[ngb_ind])
            neighb_ps_dists.append(dist)
    return faces, neighb_points, neighb_ps_dists

def get_all_solid_angles(vor, vor_ind, syms, filter_weight=None):
    """ Return solid angles from voronoi partition """
    faces, neighb_points, dists = get_all_faces_voronoi(vor, vor_ind)
    solid_angles = []
    ngb_els = []
    for face, ngb_point  in list(zip(faces, neighb_points)):
        sa = solid_angle(vor.points[0], face)
        solid_angles.append(sa)
        ngb_els.append(syms[ngb_point])
        
    if filter_weight:
        sa_max = max(solid_angles)
        # expression to filter out due to weight
        mask = np.array(solid_angles) > filter_weight * sa_max
        ngb_els = np.array(ngb_els)[mask]
        solid_angles = np.array(solid_angles)[mask]
        neighb_points = np.array(neighb_points)[mask]
        dists = np.array(dists)[mask]


    return list(solid_angles), list(neighb_points), list(dists), list(ngb_els)


def get_syms_from_indeces_vor(vor, chem_syms, ngb_out, ase_ind):
    """ Find the list matching point index with element """
    i_ngb, j_ngb, d_ngb, D_ngb = ngb_out
    mask = i_ngb == ase_ind
    symbols = [chem_syms[ase_ind]]
    symbols += [chem_syms[i] for i in j_ngb[mask]]
    return symbols

def get_inds_from_indeces_vor(atoms, vor, chem_syms, ngb_out, ase_ind, ref_pos):
    """ Find the list matching point index with element """
    # the problem with the neigbors list is that i,j are not ordered
    # e.g. it is not the shortest distance
    pos_vor = wrap_positions(vor.points + ref_pos, atoms.cell, pbc=True)
    ase_inds = [ind_from_pos(atoms, pos) for pos in pos_vor]

    # i_ngb, j_ngb, d_ngb, D_ngb = ngb_out
    # mask = i_ngb == ase_ind
    # ase_inds = [ase_ind]
    # ase_inds += [i for i in j_ngb[mask]]
    return ase_inds


def find_interstitials(atoms, ind_init, 
                       unit_cell=None, 
                       repeat=None, 
                       filter_out=True,
                       ion=None):
    """ Returns pymatgen object containing all interstitial positions 
    
    Parameters
    ------------
    atoms : ase-atoms object
    unit_cell : ase-atoms object
        If supplied, unit cell of the supercell given as atoms
    repeat : list[int]
        unit cell repititions to go from unit_cell*repeat = atoms (supercell)
        
    Notes
    ------
    The idea with supplying the unit cell is to make the voronoi analysis more
    efficient, i.e. for the chevrel supercell this gives a speedup of 1000x
    
    """
    if unit_cell and repeat:
        atoms = unit_cell.copy()
    
    adapt = AseAtomsAdaptor()
    structure = adapt.get_structure(atoms)

    framework = list(structure.symbol_set)
    get_voronoi = TopographyAnalyzer(structure, framework, [], check_volume=False)
    get_voronoi.cluster_nodes(tol=.8)
    get_voronoi.remove_collisions(min_dist=1)
    
    if filter_out:
        if ion is None:
            ion = atoms.get_chemical_symbols()[ind_init]
        interstitions = filter_pm_interstitials(
            atoms, get_voronoi.vnodes, ind_init, ion)
    else:
        interstitions = []
        for vnode in get_voronoi.vnodes:
            interstitions.append(
                atoms.cell.cartesian_positions(vnode.frac_coords))
    
    if unit_cell and repeat:
        print("initial amout interstitions uc: ", len(interstitions))
        atoms_temp = Atoms(['X']*len(interstitions),  # mock
                           positions=interstitions,
                           cell=atoms.get_cell(),
                           pbc=[1, 1, 1])
        atoms_temp = atoms_temp.repeat(repeat)
        interstitions = atoms_temp.get_positions()
        
    

    return np.array(interstitions)


def filter_pm_interstitials(atoms, pm_interstitions, ind_init, ion):
    """ Remove interstitials with unrealistic void space or bond lengths """
    interstitions = []
    ps = atoms.get_positions()
    chem_syms = atoms.get_chemical_symbols()  
    el = ion  # element of interest
    for vnode in pm_interstitions:
        p_vnode = atoms.cell.cartesian_positions(vnode.frac_coords)
        _, dists = get_distances(p_vnode, ps, cell=atoms.cell, pbc=True)
        dists_inds = sorted([[dist, i] for i, dist in enumerate(dists[0])])
        ngb_list = dists_inds[:vnode.coordination]
        # filter if d smaller than 65% of covalent distance (found after testing)
        if any([d < covalent_dist(el, chem_syms[ind]) * 0.65
                for d, ind in ngb_list 
                if chem_syms[ind] != el]): # substituting itself is fine!
                continue
        else:
            interstitions.append(
                atoms.cell.cartesian_positions(vnode.frac_coords))

    return np.array(interstitions)


def covalent_dist(sym1, sym2):
    cov_r1 = covalent_radii[atomic_numbers[sym1]]
    cov_r2 = covalent_radii[atomic_numbers[sym2]]
    return cov_r1 + cov_r2
    
    
    
def topology_voronoi_path(atoms, ind_init, ind_final, 
                          unit_cell=None,
                          repeat=None,
                          max_steps=10,
                          tol=1e-6):
    """ Walks through all the steps 
    
    Parameters
    ------------
    tol : float
        tolerance used to detect if interstition overlaps with bridge position
    unit_cell : ase.atoms
        Providing the unit cell and the repeat parameter will lead to do symm-
        etry pymatgen analysis on the unit cell (you dont want to miss out on
                                                 that speed up!)
    """
    atoms = atoms.copy()
    ion = atoms.symbols[ind_init]
    # first get all interstitions
    interstitions = find_interstitials(atoms, ind_init,
                                       unit_cell=unit_cell,
                                       repeat=repeat,
                                       ion=ion)
    print(f"all interstitions found: {len(interstitions)} - moving to faces")
    # now traverse from ind_init to ind_final
    # possible faces to traverse through
    path_list = []
    ps = atoms.get_positions()
    p_final = ps[ind_final]
    ind_init_temp = ind_init
    

    for step in range(max_steps):
        if step == max_steps - 1:
            break
            raise ValueError(f"Max number steps {max_steps} reached")
            
        if step == 0:
            cur_pos = ps[ind_init]
            
        # the initial step starting from ind_init
        r_crits, f_chebyshevs, f_els, f_d_el_cheb, f_inds = find_possible_faces(
            atoms, ind_init, ind_final, ion)
        

        # the intermediate steps until reaching p_final
        # pick possible faces to traverse through
        cheb_points, cheb_inds, cheb_final_d, cur_final_d = choose_closest_points(
            atoms, cur_pos, p_final, points=f_chebyshevs)
        ngbs_str, dists = neighbors_from_position(atoms, cur_pos, ion)
        
        # now all of these are poosible indices, choose the one minimizing dist
        if len(cheb_final_d) == 0:
            # very brute force, need to find something better
            path_list.append("final")
            break
        cheb_ind = cheb_inds[np.argmin(cheb_final_d)]
        if step == 0:
            path_list.append([f"initial"])
        
        ngb_els, dists = neighbors_from_position(
                atoms, f_chebyshevs[cheb_ind], ion)
        
        # check "bridge-positions" for possible transition states
        bridge_ps = bridge_ps_is_ts(
            atoms, 
            f_chebyshevs[cheb_ind],
            f_inds[cheb_ind],
            p_final,
            ion,
            ts_cnn=len(ngb_els),
            path_list=path_list)
        
        if bridge_ps:
            bridge_p = bridge_ps
            path_list += [bridge_p]
            cur_bridge_face_p = bridge_p[2]
            if len(interstitions) != 0:
                _, d = get_distances(
                    cur_bridge_face_p, interstitions, cell=atoms.cell, pbc=True)
                # it can happen that bridge position is actually interstition
                # in that case we remove the corresponding interstition
                if any(d[0] < tol):
                    interstitions = np.delete(
                        interstitions, np.argmin(d[0]), axis=0)
        else:
            # get the number of neighbors in 3D and not only on face
            # ngbs_str, dists = neighbors_from_position(
            #     atoms, p_cur=f_chebyshevs[cheb_ind], ion=ion)
            
            path_list.append(["face_center",
                              r_crits[cheb_ind], # r_crit
                              f_chebyshevs[cheb_ind], #  position
                              ngb_els, #[str(e) for e in f_els[cheb_ind]],  # ngb_strs
                              dists,] # f_d_el_cheb[cheb_ind]]  # dists
                             )
            cur_bridge_face_p = f_chebyshevs[cheb_ind]
            # also check if it overlaps with an interstition
            if len(interstitions) != 0:
                _, d = get_distances(
                    cur_bridge_face_p, interstitions, cell=atoms.cell, pbc=True)
                # it can happen that bridge position is actually interstition
                # in that case we remove the corresponding interstition
                if any(d[0] < 0.15):
                    interstitions = np.delete(
                        interstitions, np.argmin(d[0]), axis=0)
            

        # now move to next position
        # either interstitial site or final site
        # always try if it can move directly to final site
        if len(interstitions) != 0:
            pos_inters, inds_inters, points_final_d, cur_final_d = choose_closest_points(
                atoms, cur_bridge_face_p, p_final, points=interstitions)
        else:
            # check if ion occupies a site that would be possible
            ion_ps = [atom.position for atom in atoms 
                      if atom.symbol == ion
                      and atom.index not in [ind_init, ind_final]]
            ion_ps = np.array(ion_ps)
            pos_inters, inds_inters, points_final_d, cur_final_d = choose_closest_points(
                atoms, cur_bridge_face_p, p_final, points=ion_ps)
            if len(pos_inters) > 0:
                path_list.append(["ion_in_path"])
                return path_list
        
        # also check how far to new position
        inds_inters_consider = []
        for i in range(len(pos_inters)):
            _, d = get_distances(
                cur_bridge_face_p, pos_inters[i], cell=atoms.cell, pbc=True)
            if d < cur_final_d:
                inds_inters_consider.append(inds_inters[i])
            
        if len(inds_inters_consider) == 0:
            path_list.append(["final"])
            break
        else:
            # choose the one closest to cur_pos
            _, cur_inters_d = get_distances(
                cur_bridge_face_p, interstitions[inds_inters_consider],
                cell=atoms.cell, pbc=True)
            cur_inters_d = cur_inters_d[0]
            poss_inter_ind = inds_inters_consider[np.argmin(cur_inters_d)]
            ind_init = (ind_init_temp, interstitions[poss_inter_ind])
            cur_pos = interstitions[poss_inter_ind]
            ngbs_str, dists = neighbors_from_position(atoms, cur_pos, ion)
            path_list.append(["interstition",
                             int(poss_inter_ind),  # r_crit - just a mock value
                             cur_pos, # position
                             ngbs_str,
                             dists,
                             ])

    return path_list


def neighbors_from_position(atoms, p_cur, ion, return_all=False):
    """ Count CNN around specific position """
    atoms = atoms.copy()
    del atoms[[atom.index for atom in atoms if atom.symbol == ion]]
    atoms_temp = atoms.copy()
    atoms_temp.append(Atom('X', position=p_cur))
    
    ind_x = len(atoms_temp) - 1
    vor , angles, neighb_points, dists, ngb_els, syms = count_cnn(
        atoms_temp, ind_x)
    
    ngbs_str = [str(e) for e in ngb_els]
    
    if return_all:
        return vor , angles, neighb_points, dists, ngbs_str, syms

    return ngbs_str, dists


def count_cnn(atoms, ind):
    """ neighbor counting using voronoi tesselation and solid angles """
    ref_pos = voronoi_ref_pos(atoms, ind)
    chem_syms = atoms.get_chemical_symbols()
    vor, ind, ngb_out = build_voronoi_partition(atoms, ref_pos)
    ase_inds = get_inds_from_indeces_vor(atoms, vor, chem_syms, ngb_out, ind, ref_pos)
    # symbols matching each voronoi index
    syms = [chem_syms[ase_inds[i]] for i in range(len(vor.points))]
    
    angles, neighb_points, dists, ngb_els = get_all_solid_angles(
        vor, 0, syms, filter_weight=0.5)

    return vor, angles, neighb_points, dists, ngb_els, syms


def bridge_ps_is_ts(atoms, p_ts, inds_ngbs, p_final, ion, ts_cnn, path_list, cl_tol=0.5):
    """ Check if bridge-positions are possible transition states 
    
    Parameters
    ------------
    ts_cnn : int
        CNN at transition state
    cl_tol : float
        Cluster tolerance value for considering bridge position equal
    
    """
    bridge_ps = []
    atoms = atoms.copy()
    for ind_1, ind_2 in list(zip(inds_ngbs, np.roll(inds_ngbs, -1))):
        atoms_temp = atoms.copy()
        bridge_p = path_midpoint(atoms, ind_1, ind_2)
        
        # check if overlaps with ts found, remove collisions
        _, cur_final_d = get_distances(
                p_ts, bridge_p, cell=atoms.cell, pbc=True)
        if cur_final_d[0] < 1e-3:
            continue

        atoms_temp.append(Atom('X', position=bridge_p))

        ind_x = len(atoms_temp) - 1
        ngb_els, dists = neighbors_from_position(atoms, bridge_p, ion)
        # _ , angles, neighb_points, dists, ngb_els = count_cnn(
        #     atoms_temp, ind_x)

        # r_crit in bridge position
        dist_b = atoms_temp.get_distance(ind_1, ind_2, mic=True)
        chem_syms = atoms_temp.get_chemical_symbols()
        r_crit = r_crit_bridge(dist_b, chem_syms[ind_1], chem_syms[ind_2])
        
        # now check if bridge position is accepted
        # TODO: check covalent bond length to all neighbors
        # all neighboring elements need to be same as edge element
        # that means anion framework is the same all the time
        cur_bridge_ps = [entry[2] for entry in bridge_ps]
        if cur_bridge_ps:
            _, cur_final_d = get_distances(
                bridge_p, cur_bridge_ps, cell=atoms.cell, pbc=True)
            if any(cur_final_d[0] < cl_tol):
                continue
        
        if all([ngb_el == chem_syms[ind_1] for ngb_el in ngb_els]):
            # the coordination has to increase
            # this might not always be a good/robust choice, needs testing
            b_cnn = len(ngb_els)
            if b_cnn <= ts_cnn:
                continue

            _, p_ts_f_d = get_distances(
                p_ts, p_final, cell=atoms.cell, pbc=True)
            _, p_b_f_d = get_distances(
                bridge_p, p_final, cell=atoms.cell, pbc=True)
            # only append if bridge is closer to final position
            if p_ts_f_d[0] < p_b_f_d[0]:
                continue
            
            bridge_ps.append(['bridge',
                              r_crit,
                              bridge_p,
                              [str(e) for e in ngb_els],
                              dists])
        # also check if bridge_ps clusters with other bridge_ps
        # that is needed since no voronoi clustering at bridge positions
    # choose the one with largest critical radius?
    if bridge_ps:
        bridge_ps = sorted(bridge_ps, key=itemgetter(1))[0]
    
    return bridge_ps
                

def r_crit_bridge(length, el_1, el_2):
    ox_states = {**max_ox_state_metals, **non_metal_ox_states}
    r_el_1 = get_radius_ion(el_1, ox_states[el_1])
    r_el_2 = get_radius_ion(el_2, ox_states[el_2])
    r_crit = (length - r_el_1 - r_el_2)/2
    return r_crit


def choose_closest_points(atoms, cur_pos, p_final, points):
    """ Identify points from cur_pos that minimuze distance to p_final """
    _, cur_final_d = get_distances(
        p_final, cur_pos, cell=atoms.cell, pbc=True)
    _, points_final_d = get_distances(
        p_final, points, cell=atoms.cell, pbc=True)
    cur_final_d = cur_final_d[0]
    points_final_d = points_final_d[0]

    inds = np.where(cur_final_d - points_final_d > 0)[0]
 
    return points[inds], inds, points_final_d[inds], cur_final_d
        

def voronoi_ref_pos(atoms, ind_init):
    """ Origin of building voronoi partition around 
    
    Notes
    --------
    It seems intuitive to choose the `path_midpoint` since that would be
    the center. In that case though, it might be difficult to map back
    matching chemical elements, as this is based on distances to this
    reference point (see `get_syms_from_indeces_vor`). This might cause
    some numerical ambiguity, needs more thinking.
    """
    # midpoint_path = path_midpoint(atoms, ind_init, ind_final)
    p_init = atoms.get_positions()[ind_init]
    return p_init

def find_possible_faces(atoms, ind_pos, ind_final, ion):
    """ find all possible faces/bottlenecks surrounding position 

    Parameters
    --------------
    atoms : ase.atoms
    ind_pos : int or tuple(int, ndarray)
        Indice of atom environment to investigate for faces/bottlenecks.
        If instead a tuple(int, ndarray) is given, the int indice atom
        will be moved to the new position according to the ndarray and 
        the environment at the new position will be investigated. 
        Can be tuple or list
    """
    # first only consider neighbor points based on solid angle filter
    atoms_init = atoms.copy()
    atoms = atoms.copy()

    if isinstance(ind_pos, int):
        ref_pos = voronoi_ref_pos(atoms, ind_pos)
    elif isinstance(ind_pos[0], int) and isinstance(ind_pos[1], 
                                                    (np.ndarray, list)):
        # translate initial atom to new position
        ind_coord = ind_pos[1]
        ind_pos = ind_pos[0]
        ps = atoms.get_positions()
        ps[ind_pos] = ind_coord
        atoms.set_positions(ps)
        ref_pos = voronoi_ref_pos(atoms, ind_pos)

    # remove final atom and be aware of indice changes
    del atoms[ind_final]
    ind_pos = ind_from_pos(atoms, ref_pos)
    
    
    vor, angles, neighb_points, dists, ngb_els, syms = neighbors_from_position(
        atoms, ref_pos, ion, return_all=True)

    if len(neighb_points) < 4:  # can not create simplex
        for filter_weight in np.arange(0.3, 0.5, 0.05)[::-1]:
            angles, neighb_points, dists, ngb_els = get_all_solid_angles(
                vor, 0, syms, filter_weight=filter_weight)
            if len(neighb_points) > 3:
                break

    if len(neighb_points) < 4:
        raise ValueError("Can not create simplex, too little ngb points")

    # find r_critical and center of all faces
    r_crits, f_chebyshevs, f_els, f_d_el_cheb, polyhedra_3D = investigate_ngb_faces(
        vor, neighb_points, ngb_els, ref_pos)
    
    # wrap into cell
    f_chebyshevs = wrap_positions(f_chebyshevs, atoms.cell, pbc=True)
    polyhedra_3D_inds = []
    for polyhedron_3D in polyhedra_3D:
        polyhedron_3D = wrap_positions(polyhedron_3D, atoms.cell, pbc=True)
        facet_inds = [ind_from_pos(atoms_init, pos) for pos in polyhedron_3D]
        polyhedra_3D_inds.append(facet_inds)

    return r_crits, f_chebyshevs, f_els, f_d_el_cheb, polyhedra_3D_inds

def ind_from_pos(atoms, ref_pos):
    """ Return indice of atom closes to ref_pos """
    ps = atoms.get_positions()
    _, dists = get_distances(np.array(ref_pos), ps, cell=atoms.cell, pbc=True)
    return np.argmin(dists)

class MinimumPath:
    
    def __init__(self, vertices, ridge_vertices):
        self.vertices = np.array(vertices)
        self.ridge_vertices = np.array(ridge_vertices)
    
    def vertice_neighbors(self, ind):
        neighbors = []
        #ngb_inds = self.ridge_vertices[self.ridge_vertices[:, 0] == ind]
        for vertice in self.ridge_vertices:
            if ind in vertice:
                list_ind = vertice.index(ind)
                list_ind_left = list_ind - 1
                
                if list_ind_left < 0:
                    list_ind_left = -1
                list_ind_right = list_ind + 1
                if list_ind_right == len(vertice):
                    list_ind_right = 0
                    
                neighbors.append(vertice[list_ind_right])
                neighbors.append(vertice[list_ind_left])
        return list(set(neighbors))
        #return ngb_inds[:, 1]
    
    def look_at_neighbors(self, cur_ind, sp_tree_list):
        vs = self.vertices

        to_remove = False
        for ngb_ind in self.vertice_neighbors(cur_ind):
            d = np.linalg.norm(vs[cur_ind] - vs[ngb_ind])
            for i, (path, length) in enumerate(sp_tree_list):
                if path[-1] == cur_ind:
                    # remove from list
                    to_remove = i
                    sp_tree_list.append([path + [ngb_ind], length + d])
                    break
        # remove after it has been updated
        if to_remove:
            del sp_tree_list[to_remove]
        return sp_tree_list
    
    def investigate_min_path(self, sp_tree_list):
        dists = [l for path, l in sp_tree_list]
        min_ind = dists.index(min(dists))
        return sp_tree_list[min_ind][0][-1]
    
    
    def path_search(self, init_ind, final_ind, max_paths=1):
        paths_found = 0
        sp_tree_set = []
        sp_tree_list = []
        
        cur_ind = init_ind
        
        for i in range(1000):
            if paths_found == max_paths:
                break
            sp_tree_list = self.look_at_neighbors(cur_ind, sp_tree_list)
            cur_ind = self.investigate_min_path(sp_tree_list)
            if cur_ind == final_ind:
                paths_found += 1
        return sp_tree_list


def visualize_path_from_path_list(atoms, p_list):
    atoms = atoms.copy()
    for p in p_list:
        if p[0] == 'face_center':
            atoms.append(Atom('He', p[2]))
        if p[0] == 'interstition':
            atoms.append(Atom('Ne', p[2]))
        if p[0] == 'bridge':
            atoms.append(Atom('X', p[2]))
            
    view(atoms)

if __name__ == "__main__":
    if 1:
        from ase.db import connect
        from ase.atoms import Atoms, Atom
        import datetime
        from iiwPBEU.battery_tools import get_scaled_supercell
        db = connect('/home/felix/python_files/DTU/mg-workflow/post_process_collect/mgdata_all_reflective.db')
        mpid = 'mp-676282'  # Chevrel, 0 - 49, 0 - 113
        #mpid = 'mp-27510' # Mg2Mn4O8, 8 - 9
        #mpid = 'mp-18900' # 0-85
        #mpid = 'mp-6700' # 48 - 51 cr-garnet
        #mpid = 'mp-864954' # 0 88
        #mpid = 'mp-1069270' # 0 10
        #mpid='mp-1199855' # 0 - 128
        #mpid = 'mp-1192094' # 6 - 161 (through bulk) or 6-139
        #mpid = 'mp-27219' # 8 - 38

        row = db.get(mpid=mpid)
        atoms = Atoms.fromdict(row.data['supercell_final'])
        unit_cell = Atoms.fromdict(row.data['optimized_discharged_unit_cell_final'])

        unit_cell_empty = Atoms.fromdict(row.data['optimized_charged_constraint_unit_cell_init'])
        supercell = Atoms.fromdict(row.data['supercell_final'])
        repeat = row.data['potneb_dic']['repeat_unitcell']
        supercell_empty = unit_cell_empty.repeat(repeat)
        unit_cell_empty = get_scaled_supercell(unit_cell, unit_cell_empty)
        supercell_scaled = get_scaled_supercell(supercell, supercell_empty)
        
        path_list = topology_voronoi_path(supercell, ind_init=0,
                                          ind_final=49, # 49, 113 
                              unit_cell=unit_cell, 
                              repeat=row.data['potneb_dic']['repeat_unitcell'])
        #view([supercell, supercell_scaled])
        # path_list = topology_voronoi_path(supercell_scaled, ind_init=48, ind_final=51, # 49, 113 
        #                       unit_cell=unit_cell_empty, 
        #                       repeat=row.data['potneb_dic']['repeat_unitcell'])
        
        # from ase.build import bulk
        # unit_cell = bulk('Cu', 'hcp', a=4)
        # unit_cell = unit_cell.repeat([2,2,1])
        # unit_cell.set_chemical_symbols(['Mg']*2 + ['Cu']*6)
        # unit_cell = unit_cell.copy()
        # supercell = unit_cell.repeat([2,2,2])
        import sys;sys.exit()
        #try delta-v2o5
        mpid = 'mp-19003' # 16-64
        row = db.get(mpid=mpid)
        unit_cell = Atoms.fromdict(row.data['optimized_discharged_unit_cell_final'])
        supercell = Atoms.fromdict(row.data['supercell_final'])



        path_list = topology_voronoi_path(supercell, ind_init=16, ind_final=80, # 49, 113 
                              unit_cell=unit_cell, 
                              repeat=row.data['potneb_dic']['repeat_unitcell'])
        import sys;sys.exit()
        from ase.io import write
        #write('Mg2Mn4O8_mp-27510_spinel.traj', atoms)
        a = datetime.datetime.now()
        
        # path_list = topology_voronoi_path(atoms, ind_init=8, ind_final=9)
        path_list = topology_voronoi_path(atoms, ind_init=1, ind_final=84, # 49 
                              unit_cell=unit_cell, 
                              repeat=row.data['potneb_dic']['repeat_unitcell'])
        b = datetime.datetime.now()
        c = b - a
        print("xecution time:", c.microseconds / 10e6 , "s")
