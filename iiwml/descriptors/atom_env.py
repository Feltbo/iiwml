"""
Script for storing descriptors describing the environment of an atom

@authors: Felix Tim Bölle <feltbo@dtu.dk>
"""
import numpy as np

from iiwml.descriptors.voronoi import get_el_coordination_voronoi,\
                                      neighbors_filter_covalent

def filter_spec_neighbors(ind, atoms, bond_list, spec_neighbors):
    """ Only return bonding to specific elements """
    bond_list_neighbor_els = []
    for pair, d in bond_list:
        el1, el2 = pair.split('-')
        el_ind = atoms.symbols[ind]
        for n_el in spec_neighbors:
            if n_el == el_ind:
                # bonding to itself, both need to be el_ind
                if all([el == n_el for el in [el1, el2]]):
                    bond_list_neighbor_els.append((pair, d))
            else:
                # otherwise it is enough if just one matches
                if any([el == n_el for el in [el1, el2]]):
                    bond_list_neighbor_els.append((pair, d))

    return bond_list_neighbor_els
                        
def get_cnn_voronoi_covalent(atoms, ind, spec_neighbors=False, verbose=False):
    """
    Coordination number of a specific element (index) in an atoms object

    Parameters
    ----------
    atoms : ase.atoms
    ind : int
        Indice of element for which coordination number will be determined
    spec_neighbors : list[str]
        Specifiy if CNN of only specified elements should be checked. E.g.
        how many oxygen sorround atom within index ind: use ['O']

    Returns
    -------
    n_neighbors : int
        number of neighbors matching the given arguments

    """
    _, bond_list = get_el_coordination_voronoi(atoms, ind=ind, cutoff=10)
    # only get covalent bonded atoms
    n_neighbors, bond_list = neighbors_filter_covalent(bond_list)

    if spec_neighbors:
        bond_list = filter_spec_neighbors(
            ind, atoms, bond_list, spec_neighbors)
        n_neighbors = len(bond_list)
    
    if verbose:
        print(bond_list)

    return n_neighbors

def get_cnn_voronoi_mindis(atoms, ind, delta=0.1, spec_neighbors=False,
                           verbose=False):
    """ Implement the minimum distance algorithm
    
    Note
    ------
    Idea taken from paper 10.26434/chemrxiv.12508229
    They show it works very well, although being so simple
    Tolerance value is also based on that paper
    """

    _, bond_list = get_el_coordination_voronoi(atoms, ind=ind, cutoff=10)

    if spec_neighbors:
        bond_list = filter_spec_neighbors(
            ind, atoms, bond_list, spec_neighbors)
    
    dists = np.array([e[1] for e in bond_list])
    d_ind_min = np.min(dists)
    d_ind_cut = (1 + delta)*d_ind_min
    bond_list_filt = []
    for i, dist in enumerate(dists):
        if dist < d_ind_cut:
            bond_list_filt.append([bond_list[i][0], dist])
    
    if verbose:
        print(bond_list_filt)

    return len(bond_list_filt)

def mindis_nn(dists, delta=0.1):
    """ Minimum distance algorithm

    Parameters
    ----------------
    dists : ndarray
        distances to all nearest neighbors
    """
    d_ind_min = np.min(dists)
    d_ind_cut = (1 + delta) * d_ind_min
    ngb_dists = dists[dists < d_ind_cut]
    
    return len(ngb_dists)
    
    
    