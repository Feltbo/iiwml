"""
Created on Mon Oct 12 09:28:49 2020

Store all voronoi related parts here

@author: Felix Tim Bölle <feltbo@dtu.dk>
"""
import numpy as np
from scipy.spatial import Voronoi
import copy

from ase.atoms import Atoms
from ase.data import atomic_numbers, covalent_radii
from ase.neighborlist import neighbor_list
from ase.visualize import view

    
def get_specific_neighbors_voronoi(atoms, element, neighbor_els):
    """ 
    Get only neighbors of specific kind using Voronoi Tesselation 
    and covalent Filter
    
    Parameters
    ------------
    atoms : ase-atoms object
    element : str
        For this element each atom coordination is checked.
    neighbor_els : list of str
        Neighboring elements, e.g. only anions considered for covalent
        bond filter
    """
    inds = [atom.index for atom in atoms if atom.symbol == element]
    amount_neighbors = []
    for ind in inds:
        _, bond_list = get_el_coordination_voronoi(atoms,
                                                   ind=ind,
                                                   cutoff=10,
                                                   verbose=False)
        n_neighbors, bond_list = neighbors_filter_covalent(bond_list,
                                                           verbose=False)
        # only include neighbors that are anions
        bond_list_neighbor_els = []
        for pair, d in bond_list:
            if any([True for el in pair.split('-') if el in neighbor_els]):
                bond_list_neighbor_els.append((pair, d))

        amount_neighbors.append(len(bond_list_neighbor_els))

    return np.array(amount_neighbors)

def get_el_coordination_voronoi(atoms, ind, cutoff = 10,
                                visualize=False, verbose=False):
    """
    Returns the coordination number of a specific element

    Parameters
    ----------
    atoms : ase-atoms object
    ind : int
        Index of element of interest.
    cutoff : float
        cutoff value for creating neighbor list
    visualize : Boolean
        Show environment of atom with ase-gui
    verbose : Boolean

    Returns
    -------
    n_neighbors : int
        How many neighbors after voronoi tesselation
    bond_list : list of tuples
        Info on which neighbors does atom have and what is the bond distance to
        these neighbors. E.g. bond_list = [('Mg-O', 2.561)] means a single
        neighbor of Mg with O and they are 2.561 apart

    """
    i,j,d,D = neighbor_list('ijdD', atoms, cutoff = cutoff)

    if visualize:
        visualize_from_neighbor_list(atoms, ind, i, j, D)

    ### Create the environment of atoms[ind]
    syms = atoms.get_chemical_symbols()
    mask = i==ind
    points = [[0, 0, 0]] # place element of interest on origin
    distances = []
    for vec in D[mask]:
        points.append(vec)

    ### VORONOI part
    symbols_nl = [syms[ind]]  # the neighbor list symbols in environment
    symbols_nl += [syms[i] for i in j[mask]]

    vor = Voronoi(points)
    rp1 = 0  # ridge point 0 is the origin or atom of interest
    n_neighbors = 0
    bond_list = []
    for ridge_points, ridge_vertices in vor.ridge_dict.items():
        if rp1 in ridge_points:

            if -1 in ridge_points:
                raise ValueError("Infinite Voronoi region, increase cut-off")

            rp2 = [v for v in ridge_points if v != rp1][0]
            sym1, sym2 = symbols_nl[rp1], symbols_nl[rp2]
            # bondlength
            bl = np.linalg.norm(d[mask][rp2 - 1])

            bond_list.append((f'{sym1}-{sym2}', bl))
            n_neighbors += 1
            if verbose:
                print("Found neighbor %d %s-%s with bondlength %5.3f"%(
                        n_neighbors,sym1, sym2, bl))

    return n_neighbors, bond_list

def neighbors_filter_covalent(bond_list, tol=1.2, verbose=False):
    """
    Only consider neighbor pairs that are covalently bonded

    Parameters
    ----------
    bond_list : list of tuples
        Comes from get_el_coordination_voronoi()
    tol : float
        Tolerance parameter on how much is considered a covalent bond. If set
        to 1 it means tolerated is everything that is equal to the covalent
        bond length of the two elements. The covalent bond length is calculated
        as adding the covalent radius of each element together.
        Default is 1.2, that is 20% on top.

    Returns
    -------
    n_neighbors : int
        How many neighbors after covlant filter
    bond_list_new : list of tuples
        Info on which neighbors does atom have and what is the bond distance to
        these neighbors. E.g. bond_list_new = [('Mg-O', 2.561)] means a single
        neighbor of Mg with O and they are 2.561 apart

    """

    bond_list_new = []
    for bond_pair, bl in bond_list:
        sym1, sym2 = bond_pair.split('-')
        cov_r1 = covalent_radii[atomic_numbers[sym1]]
        cov_r2 = covalent_radii[atomic_numbers[sym2]]
        cov_av = (cov_r1 + cov_r2) * tol

        if verbose:
            print("Bond-pair %s-%s with bondlength %5.3f, acceptable %5.3f"%(
                        sym1, sym2, bl, cov_av))

        if bl < cov_av:
            bond_list_new.append((bond_pair, bl))

    n_neighbors = len(bond_list_new)
    return n_neighbors, bond_list_new

def visualize_from_neighbor_list(atoms, ind, i, j, D):
    """
    Visualize environment after applying neighbor_list

    Parameters
    ----------
    atoms : ase-atoms object
    ind : int
        Index of interest
    i,j,D : ase-neighbor_list output
        Output from ase.neighborlist.neighbor_list

    """
    atoms = create_sphere_from_neighbor_list(atoms, ind, i, j, D)

    view(atoms)
    
def create_sphere_from_neighbor_list(atoms, ind, i, j, D):
    """ Return spehere atoms object from neighbor list """
    atoms = copy.deepcopy(atoms)

    syms = atoms.get_chemical_symbols()
    mask = i==ind

    points = [[0, 0, 0]]
    for d in D[mask]:
        points.append(d)

    symbols = [syms[ind]]
    symbols += [syms[i] for i in j[mask]]
    atoms_sphere = Atoms(symbols=symbols,
                         positions=points)
    return atoms_sphere