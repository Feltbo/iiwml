"""
Script for storing lattice descriptor functions for battery materials

@authors: Felix Tim Bölle <feltbo@dtu.dk>
"""

class LatticeFeatures:
    """ Stores all features related to cell object """
    def __init__(self, atoms):
        len_a, len_b, len_c = get_cell_lengths(atoms)
        self.len_a = len_a
        self.len_b = len_b
        self.len_c = len_c
        alpha, beta, gamma = get_cell_angles(atoms)
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma
        total_v = get_cell_volume(atoms)
        self.total_v = total_v


def get_cell_lengths(atoms):
    a, b, c, _, _, _ = atoms.get_cell_lengths_and_angles()
    return a, b, c

def get_cell_angles(atoms):
    _, _, _, alpha, beta, gamma = atoms.get_cell_lengths_and_angles()
    return alpha, beta, gamma

def get_cell_volume(atoms):
    return atoms.get_volume()
