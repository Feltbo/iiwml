# -*- coding: utf-8 -*-

"""
Compare and define similarity between two structures

@authors: Felix Tim Bölle <feltbo@dtu.dk>
"""
import numpy as np

from ase.geometry import get_distances


def structural_distortion_rmse(atoms1, atoms2):
    """ Compare the structural distortion between two atoms objects """
    p1 = atoms1.get_positions()
    p2 = atoms2.get_positions()

    D, D_len = get_distances(p1, p2, cell=atoms1.get_cell(), pbc=True)
    distort = np.sqrt(np.diagonal(D) ** 2).mean()  # rmse
    return distort
